# Ipponboard - the score board for judoka by judoka

## Motivation
*Judo is part of our lives. Therefore, Ipponboard is not just a simple display program, but developed by judoka for judoka. It is not only helpful for the people at the time table, but also for the trainers, the audience and the fighters themselves.*

*Ipponboard is not only clearly readable but also revolutionary easy and intuitive to use. Therefore, it is appreciated by clubs and organizations around the world and has been used for many years at major championships.*

## Legal stuff

This version can be used without restriction. Copying in unchanged form is permitted.

## How to contribute

If you like Ipponboard, please support its development by:

- feedback
- wishes and suggestions
- or by giving appreciative donations

*Thank you very much!*

### Contribution guidelines ###

**TODO**

### Who do I talk to? ###

**TODO
* Repo owner or admin
* Other community or team contact